<?php

namespace AppBundle\Repository;

use AppBundle\Dto\Neo as NeoDto;
use AppBundle\Entity\Neo;
use Doctrine\ORM\EntityRepository;

class NeoRepository extends EntityRepository
{
    /**
     * @param NeoDto[] $neos
     */
    public function saveNeos(array $neos)
    {
        foreach ($neos as $neo) {
            $neoEntity = new Neo();
            $neoEntity->setDate($neo->getDate())
                ->setSpeed($neo->getSpeed())
                ->setName($neo->getName())
                ->setReference($neo->getReference())
                ->setHazardous($neo->isHazardous());

            $this->getEntityManager()->persist($neoEntity);
        }

        $this->getEntityManager()->flush();
    }

    public function findBestYear($isHazardous = false)
    {
        $result = $this->getEntityManager()->createQuery("
            SELECT DATE_FORMAT(n.date, '%Y') as gyear, COUNT(n.date) AS total
            FROM AppBundle:Neo n
            WHERE n.hazardous=:hazardous
            GROUP BY gyear
            ORDER BY total DESC
        ")
            ->setParameter('hazardous', $isHazardous)
            ->setMaxResults(1)->getOneOrNullResult();
        return $result !== null ? $result['gyear'] : null;
    }

    public function findBestMonth($isHazardous = false)
    {
        $result = $this->getEntityManager()->createQuery("
            SELECT DATE_FORMAT(n.date, '%Y-%m') as gmonth, COUNT(n.date) AS total
            FROM AppBundle:Neo n
            WHERE n.hazardous=:hazardous
            GROUP BY gmonth
            ORDER BY total DESC
        ")
            ->setParameter('hazardous', $isHazardous)
            ->setMaxResults(1)->getOneOrNullResult();
        return $result !== null ? $result['gmonth'] : null;
    }

    /**
     * @param bool $isHazardous
     *
     * @return Neo
     */
    public function findFastest($isHazardous = false)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('n')
            ->from('AppBundle:Neo', 'n')
            ->orderBy('n.speed', 'DESC')
            ->where('n.hazardous = :hazardous')
            ->setParameter('hazardous', $isHazardous)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
