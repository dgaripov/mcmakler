<?php

namespace AppBundle\Services\Nasa;


use AppBundle\Dto\Neo;
use AppBundle\Dto\NeosList;
use JMS\Serializer\SerializerInterface;

class NeoDataFetcher
{
    /**
     * @var NasaApiRequestService
     */
    private $nasaApi;

    /**
     * @param NasaApiRequestService $nasaApi
     */
    public function __construct(NasaApiRequestService $nasaApi)
    {
        $this->nasaApi = $nasaApi;
    }

    /**
     * @param $params
     *
     * @return Neo[]
     */
    public function getNeosList($params)
    {
        $apiParams = [
            'start_date' => isset($params['start_date']) ? $params['start_date']->format('Y-m-d') : date('Y-m-d'),
            'end_date' => isset($params['end_date']) ? $params['end_date']->format('Y-m-d') : date('Y-m-d'),
            'detailed' => isset($params['detailed']) ? $params['detailed'] : false,
        ];

        $response = $this->nasaApi->get('feed', $apiParams);

        return $this->serialize($response->getBody());
    }

    /**
     * @param $json
     *
     * @return Neo[]
     */
    private function serialize($json)
    {
        $responseArray = json_decode($json, true);
        $result = [];
        foreach ($responseArray['near_earth_objects'] as $date => $neos) {
            $result = array_merge($result, $this->hydrateNeos($neos, \DateTime::createFromFormat('Y-m-d', $date)));
        }

        return $result;
    }

    private function hydrateNeos(array $neos, \DateTime $date) {
        $neosList = [];

        foreach ($neos as $neo) {
            $neoDto = new Neo();

            $neoDto->setName($neo['name'])
                ->setDate($date)
                ->setReference($neo['neo_reference_id'])
                ->setSpeed($neo['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'])
                ->setHazardous($neo['is_potentially_hazardous_asteroid']);

            $neosList[] = $neoDto;
        }

        return $neosList;
    }
}