<?php

namespace AppBundle\Services\Nasa;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class NasaApiRequestService
{
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     * @param string $apiKey
     */
    public function __construct(Client $client, $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
    }

    /**
     * @param string $uri
     * @param array  $params
     *
     * @return mixed|ResponseInterface
     */
    public function get($uri, $params = [])
    {
        $paramsString = http_build_query(array_merge($params, ['api_key' => $this->apiKey]));

        return $this->client->get($uri.'?'.$paramsString);
    }
}