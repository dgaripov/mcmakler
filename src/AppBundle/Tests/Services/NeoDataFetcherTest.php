<?php

namespace AppBundle\Tests\Services;

use AppBundle\Dto\Neo;
use AppBundle\Services\Nasa\NasaApiRequestService;
use AppBundle\Services\Nasa\NeoDataFetcher;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class NeoDataFetcherTest extends TestCase
{
    /**
     * @var NeoDataFetcher
     */
    private $neoDataFetcher;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->neoDataFetcher = new NeoDataFetcher($this->createApiServiceMock());
    }

    public function testGetNeosListReturnsCorrectObjectsArray()
    {
        $neos = $this->neoDataFetcher->getNeosList([
            'start_date' => new \DateTime('2017-01-05'),
            'end_date' => new \DateTime('2017-01-06'),
        ]);

        $this->assertInternalType('array', $neos, 'Fetcher result is not an array');
        $this->assertCount(3, $neos, 'Not correct amount of data hydrated');

        foreach ($neos as $neo) {
            $this->assertInstanceOf(Neo::class, $neo, 'Array contains not Neo dto object');
            $this->assertNotNull($neo->getName(), 'Name is null');
            $this->assertNotNull($neo->getSpeed(), 'Speed is null');
            $this->assertNotNull($neo->getReference(), 'Reference is null');
            $this->assertNotNull($neo->getDate(), 'Date is null');
            $this->assertInstanceOf(\DateTime::class, $neo->getDate(), 'Date field is of a wrong type');
        }

        $first = reset($neos);

        $this->assertEquals('Foo', $first->getName(), 'Name data is wrong');
        $this->assertEquals('12345', $first->getReference(), 'Reference data is wrong');
        $this->assertEquals('654.321', $first->getSpeed(), 'Speed data is wrong');
        $this->assertEquals('2017-01-05', $first->getDate()->format('Y-m-d'), 'Date is wrong');
        $this->assertEquals(false, $first->isHazardous(), 'Hazardous data should be false');

        $next = next($neos);

        $this->assertEquals(true, $next->isHazardous(), 'Hazardous data should be true');
    }

    /**
     * @return NasaApiRequestService|\PHPUnit_Framework_MockObject_MockObject
     */
    private function createApiServiceMock()
    {
        $mock = $this->createMock(NasaApiRequestService::class);

        $mock->expects($this->once())
            ->method('get')
            ->willReturn($this->createGuzzleResponseMock());

        return $mock;
    }

    /**
     * @return ResponseInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    public function createGuzzleResponseMock()
    {
        $apiResponse = $this->createMock(ResponseInterface::class);

        $apiResponse->method('getBody')
            ->willReturn(json_encode([
                'near_earth_objects' => [
                    '2017-01-05' => [
                        [
                            'name' => 'Foo',
                            'neo_reference_id' => '12345',
                            'is_potentially_hazardous_asteroid' => false,
                            'close_approach_data' => [
                                ['relative_velocity' => ['kilometers_per_hour' => '654.321']]
                            ]
                        ],
                        [
                            'name' => 'Bar',
                            'neo_reference_id' => '678910',
                            'is_potentially_hazardous_asteroid' => true,
                            'close_approach_data' => [
                                ['relative_velocity' => ['kilometers_per_hour' => '32.757']]
                            ]
                        ],
                    ],
                    '2017-01-06' => [
                        [
                            'name' => 'Baz',
                            'neo_reference_id' => '654321987',
                            'is_potentially_hazardous_asteroid' => false,
                            'close_approach_data' => [
                                ['relative_velocity' => ['kilometers_per_hour' => '300.444']]
                            ]
                        ]
                    ],
                ]
            ]));

        return $apiResponse;
    }
}
