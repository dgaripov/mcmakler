<?php

namespace AppBundle\Command;

use AppBundle\Entity\Neo;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateNeosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:update-neos')
            ->setDescription('Update NEOs from NASA API')
            ->addArgument(
                'days',
                InputArgument::OPTIONAL,
                'Number of days from today to fetch',
                3
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $days = $input->getArgument('days');

        $neos = $this->getContainer()->get('app.neo_data_fetcher')->getNeosList([
            'start_date' => new \DateTime(sprintf('-%d days', $days)),
            'end_date' => new \DateTime(),
        ]);

        $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(Neo::class)
            ->saveNeos($neos);

        $output->writeln(sprintf('%d NEOs were saved', count($neos)));
    }
}
