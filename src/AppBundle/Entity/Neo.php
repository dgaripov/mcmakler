<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="neo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NeoRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Neo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="`date`", type="datetime")
     * @Serializer\Expose()
     */
    private $date;

    /**
     * @var int
     * @ORM\Column(name="reference", type="integer")
     * @Serializer\Expose()
     */
    private $reference;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="speed", type="string")
     * @Serializer\Expose()
     */
    private $speed;

    /**
     * @var bool
     * @ORM\Column(name="hazardous", type="boolean")
     * @Serializer\Expose()
     */
    private $hazardous;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param int $reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param string $speed
     *
     * @return self
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHazardous()
    {
        return $this->hazardous;
    }

    /**
     * @param bool $hazardous
     *
     * @return self
     */
    public function setHazardous($hazardous)
    {
        $this->hazardous = $hazardous;

        return $this;
    }
}

