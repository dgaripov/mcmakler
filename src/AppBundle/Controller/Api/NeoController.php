<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Neo;
use AppBundle\Repository\NeoRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/neo")
 */
class NeoController extends Controller
{
    /**
     * @Route("/hazardous", name="api.neo.hazardous")
     *
     * @return JsonResponse
     */
    public function hazardousAction()
    {
        $neos = $this->getNeoRepository()->findBy([
            'hazardous' => true,
        ]);

        return $this->getSerializedJsonResponse($neos);
    }

    /**
     * @Route("/fastest", name="api.neo.fastest")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function fastestAction(Request $request)
    {
        $neo = $this->getNeoRepository()->findFastest($this->isHazardousRequested($request));

        return $this->getSerializedJsonResponse($neo);
    }

    /**
     * @Route("/best-year", name="api.neo.best-year")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function bestYearAction(Request $request)
    {
        $neo = $this->getNeoRepository()->findBestYear($this->isHazardousRequested($request));

        return $this->getSerializedJsonResponse($neo);
    }


    /**
     * @Route("/best-month", name="api.neo.best-month")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function bestMonthAction(Request $request)
    {
        $neo = $this->getNeoRepository()->findBestMonth($this->isHazardousRequested($request));

        return $this->getSerializedJsonResponse($neo);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isHazardousRequested(Request $request)
    {
        return $request->query->get('hazardous', false) === 'true';
    }

    /**
     * @param array|object $data
     *
     * @return JsonResponse
     */
    private function getSerializedJsonResponse($data)
    {
        return new JsonResponse($this->get('jms_serializer')->serialize($data, 'json'), 200, [], true);
    }

    /**
     * @return NeoRepository
     */
    private function getNeoRepository()
    {
        return $this->getDoctrine()->getManager()->getRepository(Neo::class);
    }
}
