<?php

namespace AppBundle\Dto;

class Neo
{
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var int
     */
    private $reference;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $speed;

    /**
     * @var bool
     */
    private $hazardous;

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param int $reference
     *
     * @return self
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param string $speed
     *
     * @return self
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHazardous()
    {
        return $this->hazardous;
    }

    /**
     * @param bool $hazardous
     *
     * @return self
     */
    public function setHazardous($hazardous)
    {
        $this->hazardous = $hazardous;

        return $this;
    }
}

