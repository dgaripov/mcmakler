README
========================

#### Installation:
- `composer install`
- `bin/console doctrine:schema:create`

#### Fetching data:
- `bin/console app:update-neos`

#### Tests:
- `make test`

Unfortunately I had no time for tests so I wrote only one unit test.

I chose Symfony Framework because I don't have to implement or assemble my own framework from composer components.
So I can easier and faster do my task.
Also, other developers can dive into code much faster in familiar environment.

