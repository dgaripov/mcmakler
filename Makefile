clean_cache:
	rm -Rf var/cache/*
	rm -Rf var/logs/*

test:
	make clean_cache
	export SF_DEBUG=0 && vendor/bin/phpunit -c phpunit.xml.dist